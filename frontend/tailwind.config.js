import { colors } from './carbon-colors'

/** @type {import("tailwindcss"}.Config */
const config = {
  content: ['./src/**/*.{ts,tsx}'],

  theme: {
    fontFamily: {
      sans: ['var(--font-ibm-serif)'],
      serif: ['var(--font-ibm-sansserif)'],
      mono: ['var(--font-ibm-mono)'],
    },
    extend: {
      keyframes: {
        'jump-in': {
          '0%': {
            transform: 'scale(0%)',
          },
          '80%': {
            transform: 'scale(120%)',
          },
          '100%': {
            transform: 'scale(100%)',
          },
        },
        'jump-out': {
          '0%': {
            transform: 'scale(100%)',
          },
          '20%': {
            transform: 'scale(120%)',
          },
          '100%': {
            transform: 'scale(0%)',
          },
        },
      },
      'fade-down': {
        '0%': {
          opacity: '0',
          transform: 'translateY(-2rem)',
        },
        '100%': {
          opacity: '1',
          transform: 'translateY(0)',
        },
      },

      animation: {
        'jump-in': 'jump-in .5s both',
        'jump-out': 'jump-out .5s both',
      },
      boxShadow: {
        'carbon-tile-top': `inset 1px 0px 0px ${colors.gray['20']},inset 0px 0px 0px ${colors.gray['20']},inset 0px 1px 0px ${colors.gray['20']}`,
        'carbon-tile-bottom': `1px 1px 0px ${colors.gray['20']}`,
      },
      colors: {
        carbon: {
          ...colors,
        },
      },
    },
  },
  plugins: [],
}

module.exports = config
