/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    sassOptions: {},
    // Allows image optimization
    images: {
        remotePatterns: [{
            hostname: 'img.pokemondb.net'
        }]
    },
    webpack: (config, context) => {
        // Allows import .graphql|.gql files
        config.module.rules.push({
            test: /\.(graphql|gql)/,
            exclude: /node_modules/,
            loader: "graphql-tag/loader"
        })

        return config
    }
};

export default nextConfig;
