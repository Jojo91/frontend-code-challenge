/** @type {import("eslint").Linter.Config} */
module.exports = {
  root: true,
  env: { browser: true },
  extends: ['next/core-web-vitals'],
  plugins: ['react-refresh', 'import', 'eslint-comments'],
  rules: {
    "import/order": "error"
  }
}
