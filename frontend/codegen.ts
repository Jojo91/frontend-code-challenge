import type { CodegenConfig } from '@graphql-codegen/cli'

const config: CodegenConfig = {
  schema: 'http://localhost:4000/graphql',
  documents: ['src/**/*.{ts,tsx,graphql}'],
  generates: {
    './src/__generated__/': {
      preset: 'client',
      plugins: [
        {
          add: {
            content: '// @ts-nocheck',
          },
        },
      ],
      presetConfig: {
        gqlTagName: 'gql',
        fragmentMasking: {
          // Default masking function is named `useFragment`.
          // However, it is not a hook, so it does not need to adhere to hook rules.
          // Rename it to not confuse linter, or fellow colegues.
          unmaskFunctionName: 'getFragmentData',
        },
      },
    },
  },
  ignoreNoDocuments: true,
}

export default config
