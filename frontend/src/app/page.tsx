import PokemonLayout from '@/features/pokemons/PokemonLayout'

const Home = async () => {
  return (
    <>
      <div className='bg-black flex items-end h-1/5'>
        <h1 className='text-5xl font-light text-carbon-gray-10 m-6'>Pokedex</h1>
      </div>

      <main className='flex flex-col h-4/5 '>
        <PokemonLayout />
      </main>
    </>
  )
}

export default Home
