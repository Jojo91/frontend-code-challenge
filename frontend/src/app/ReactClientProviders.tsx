import type { PropsWithChildren } from 'react'
import { ToastContainer } from 'react-toastify'
import ApolloProvider from '@/lib/ApolloProvider'

const ReactClientProviders = ({ children }: PropsWithChildren) => (
  <ApolloProvider>
    {children}
    <ToastContainer />
  </ApolloProvider>
)

export default ReactClientProviders
