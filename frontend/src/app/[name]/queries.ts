import { gql } from '@/__generated__'

export const POKEMON_DETAIL_QUERY = gql(/* GraphQL */ `
  query PokemonDetail($name: String!) {
    pokemonByName(name: $name) {
      id
      name
      maxHP
      maxCP
      image
      sound
      classification
      number

      evolutions {
        id
        ...PokemonEvolution_Pokemon
      }

      attacks {
        fast {
          name
          ...PokemonAttack_Attack
        }
        special {
          name
          ...PokemonAttack_Attack
        }
      }

      evolutionRequirements {
        name
        amount
      }

      height {
        maximum
        minimum
      }
      weight {
        maximum
        minimum
      }
      weaknesses
      types
      isFavorite
      resistant
      fleeRate
    }
  }
`)
