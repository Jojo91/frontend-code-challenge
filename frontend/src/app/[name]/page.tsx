import { RainDrop, AssemblyReference } from '@carbon/react/icons'
import clsx from 'clsx'
import { unstable_noStore as noStore } from 'next/cache'
import { notFound } from 'next/navigation'
import { POKEMON_DETAIL_QUERY } from './queries'
import type { NextPageProps } from '@/common/types'
import apolloClient from '@/lib/apolloClient'

import Fact from '@/common/components/Fact/Fact'
import Image from '@/common/components/Image'
import Title from '@/common/components/Title'

import PlaySoundButton from '@/features/pokemons/PlaySoundButton'
import AttackItem from '@/features/pokemons/AttackItem'
import EvolutionItem from '@/features/pokemons/EvolutionItem'
import PokemonTypeList from '@/features/pokemons/PokemonTypeList'
import FavoriteButton from '@/features/pokemons/FavoriteButton'

type Params = {
  name: string
}

const getPokemon = async (name: string) => {
  /**
   * I don't want to cache Pokémon data since we can favorite it. I want this information to be accurately displayed.
   * https://nextjs.org/docs/app/api-reference/functions/unstable_noStore
   */
  noStore()

  const {
    data: { pokemonByName: pokemon },
  } = await apolloClient().query({
    query: POKEMON_DETAIL_QUERY,
    variables: { name },
  })

  return pokemon
}

const PokemonDetail = async (props: NextPageProps<Params>) => {
  const pokemon = await getPokemon(props.params.name)

  // If the user enters a nonsensical name in the parameter, redirect to a "not found" page.
  if (pokemon === undefined || pokemon === null) {
    notFound()
  }

  const isFinalEvolution = pokemon.evolutions.length === 0

  return (
    <>
      <div className='bg-black flex items-end  h-1/5'>
        <div className='m-6 flex flex-col gap-1'>
          <span className='text-md text-carbon-gray-30 -mb-2'>
            {pokemon?.classification}
          </span>

          <div className='flex gap-4'>
            <h1 className='text-5xl font-light text-carbon-gray-10'>
              {pokemon?.name}
            </h1>

            <div className='flex items-center gap-2'>
              <PlaySoundButton url={pokemon.sound} />
              <FavoriteButton
                pokemonId={pokemon.id}
                isFavorite={pokemon.isFavorite}
                rsc
                light
              />
            </div>
          </div>

          <PokemonTypeList data={pokemon.types} />
        </div>
      </div>

      <main className='flex flex-col h-4/5 bg-carbon-gray-10 overflow-y-auto px-4 py-8'>
        <div className='grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-16 container mx-auto'>
          <div className='flex flex-col items-center justify-start gap-4 md:gap-8'>
            <div className='shadow-carbon-tile-top'>
              <Image
                src={pokemon?.image}
                alt={`${pokemon?.name}-image`}
                className='relative w-56 h-56 m-[1px]  md:w-96 md:h-96 shadow-carbon-tile-bottom'
              />
            </div>

            <div>
              <div className='flex flex-row gap-4'>
                <div className='flex flex-row text-4xl items-center gap-1'>
                  <div className='relative text-red-600'>
                    <span className='absolute text-xs left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2 font-bold'>
                      HP
                    </span>
                    <RainDrop size={60} />
                  </div>
                  {pokemon.maxHP}
                </div>

                <div className='flex flex-row text-4xl items-center gap-1'>
                  <div className='relative text-purple-900'>
                    <span className='absolute text-xs left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2 font-bold'>
                      CP
                    </span>
                    <AssemblyReference size={60} />
                  </div>
                  {pokemon.maxCP}
                </div>
              </div>
            </div>

            <div className='grid grid-cols-1 lg:grid-cols-2 gap-8'>
              <Fact title='Weight'>
                <div className='text-3xl flex flex-row  gap-1'>
                  <span>{pokemon.weight.minimum}</span>
                  <span>-</span>
                  <span>{pokemon.weight.maximum}</span>
                </div>
              </Fact>

              <Fact title='Height'>
                <div className='text-3xl'>
                  <div className='flex flex-row gap-1'>
                    <span>{pokemon.height.minimum}</span>
                    <span>-</span>
                    <span>{pokemon.height.maximum}</span>
                  </div>
                </div>
              </Fact>

              <Fact title='Flee Rate'>
                <div className='text-3xl'>
                  {(pokemon.fleeRate * 100).toFixed(0)} %
                </div>
              </Fact>

              <Fact title='Evolution Requirement'>
                <div
                  className={clsx('text-3xl', {
                    'text-emerald-700': isFinalEvolution,
                  })}
                >
                  {isFinalEvolution ? (
                    'Final evolution'
                  ) : (
                    <div className='flex gap-1'>
                      <span>{pokemon.evolutionRequirements?.amount}</span>
                      <span>x</span>
                      <span>{pokemon.evolutionRequirements?.name}</span>
                    </div>
                  )}
                </div>
              </Fact>
            </div>
          </div>

          <div className='flex flex-col items-center gap-8'>
            <div className='flex flex-col gap-2 w-full'>
              <Title>Weaknesses</Title>
              <PokemonTypeList
                data={pokemon.weaknesses}
                tile
              />
            </div>

            <div className='flex flex-col gap-2 w-full'>
              <Title>Resistances</Title>
              <PokemonTypeList
                data={pokemon.resistant}
                tile
              />
            </div>

            <div className='flex flex-col gap-2 w-full'>
              <Title>Fast Attacks</Title>
              <div className='grid grid-cols-3'>
                {pokemon.attacks.fast.map((attack) => (
                  <AttackItem
                    key={attack.name}
                    data={attack}
                  />
                ))}
              </div>
            </div>

            <div className='flex flex-col gap-2 w-full'>
              <Title>Special Attacks</Title>
              <div className='grid grid-cols-3 w-full'>
                {pokemon.attacks.special.map((attack) => (
                  <AttackItem
                    key={attack.name}
                    data={attack}
                  />
                ))}
              </div>
            </div>

            {isFinalEvolution ? null : (
              <div className='flex flex-col gap-2 w-full'>
                <Title>Evolutions</Title>

                <div className='flex flex-col'>
                  {pokemon.evolutions.map((evolution) => (
                    <EvolutionItem
                      data={evolution}
                      key={evolution.id}
                    />
                  ))}
                </div>
              </div>
            )}
          </div>
        </div>
      </main>
    </>
  )
}

export default PokemonDetail
