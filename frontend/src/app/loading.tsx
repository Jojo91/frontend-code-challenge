'use client'
import { SkeletonPlaceholder } from '@carbon/react'

// Why client, because skeletons depends on react context :(
const Loading = () => {
  const placeholders = [...Array(5).keys()].map((key) => (
    <SkeletonPlaceholder
      key={key}
      className='h-24 w-full'
    />
  ))

  return (
    <>
      <div className='bg-black flex items-end  h-1/5'>
        <div className='m-6 flex flex-col'>
          <SkeletonPlaceholder className='h-12 w-64' />
        </div>
      </div>

      <main className='flex flex-col h-4/5 bg-carbon-gray-10 overflow-y-auto px-4 py-8'>
        <div className='flex flex-col gap-4'>{placeholders}</div>
      </main>
    </>
  )
}

export default Loading
