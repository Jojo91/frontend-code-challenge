'use client'

import { useEffect, useMemo, useRef, useState } from 'react'

const useAudio = (src: string) => {
  const audioRef = useRef(typeof Audio === 'undefined' ? null : new Audio(src))
  const [isPlaying, setIsPlaying] = useState(false)
  const [isError, setIsError] = useState(false)

  return useMemo(() => {
    const play = () => {
      if (audioRef.current === null) return

      setIsPlaying(true)
      setIsError(false)

      audioRef.current.play().catch(() => {
        setIsError(true)
      })

      audioRef.current.onended = () => {
        setIsPlaying(false)
      }
    }

    return {
      isPlaying,
      isError,
      play,
    }
  }, [isError, isPlaying])
}

export default useAudio
