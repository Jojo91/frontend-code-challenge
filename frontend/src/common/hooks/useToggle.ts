import { useCallback, useState } from 'react'

const useToggle = (initialState: boolean = false) => {
  const [value, setValue] = useState(initialState)

  const toggle = useCallback(() => {
    setValue((x) => !x)
  }, [])

  return [value, toggle] as const
}

export default useToggle
