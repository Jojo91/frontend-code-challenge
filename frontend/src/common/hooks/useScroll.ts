import { useCallback, useEffect, useMemo, useRef, useState } from 'react'

type ScrollBehaviour = 'smooth' | 'auto' | 'instant'
type Options = {
  behaviour?: ScrollBehaviour
  showAfter?: number
}

const useScroll = ({ showAfter = 300, behaviour = 'smooth' }: Options = {}) => {
  const scrollerRef = useRef<HTMLElement | null>(null)
  const [show, setShow] = useState(false)

  const setScrollerRef = (element: HTMLElement | null) => {
    scrollerRef.current = element
  }

  const showScrollButtonElement = useCallback(() => {
    if (scrollerRef.current === null) return

    setShow(scrollerRef.current.scrollTop > showAfter)
  }, [showAfter])

  const gotTop = useCallback(
    (localBehaviour?: ScrollBehaviour) => {
      if (scrollerRef.current === null) return

      scrollerRef.current.scroll({
        top: 0,
        behavior: localBehaviour ?? behaviour,
      })
    },
    [behaviour],
  )

  useEffect(() => {
    scrollerRef.current?.addEventListener('scroll', showScrollButtonElement)

    return () => {
      scrollerRef.current?.removeEventListener(
        'scroll',
        showScrollButtonElement,
      )
    }
  }, [showScrollButtonElement])

  return useMemo(
    () => ({
      setScrollerRef,
      show,
      goTop: gotTop,
    }),
    [gotTop, show],
  )
}

export default useScroll
