import NextImage, { type ImageProps } from 'next/image'
import clsx from 'clsx'

type Props = Omit<ImageProps, 'fill'>

// Wrapper around Image from next, I like to use tailwind classes to style it
const Image = ({ className, ...rest }: Readonly<Props>) => (
  <div className={clsx('relative', className)}>
    <NextImage
      fill
      {...rest}
    />
  </div>
)

export default Image
