import type { PropsWithChildren } from 'react'
import clsx from 'clsx'

type Props = {
  className?: string
}

const Title = ({ children, className }: Readonly<PropsWithChildren<Props>>) => {
  return <h1 className={clsx('text-xl mx-2', className)}>{children}</h1>
}

export default Title
