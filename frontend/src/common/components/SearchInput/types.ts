export type SearchInputValue = string | null

export type SearchInputOnChangeHandler = (
  value: SearchInputValue,
  element: HTMLInputElement,
) => void
