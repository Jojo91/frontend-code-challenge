import type { RefAttributes } from 'react'
import { Search as CarbonSearch, type SearchProps } from '@carbon/react'
import type { SearchInputOnChangeHandler, SearchInputValue } from './types'
import { isEmptyString } from '@/common/utils/typeGuards'

type CarbonSearchProps = Omit<
  SearchProps & RefAttributes<HTMLInputElement>,
  'onChange' | 'value'
>

type Props = {
  onChange?: SearchInputOnChangeHandler
  value?: SearchInputValue
} & CarbonSearchProps

const SearchInput = ({
  onChange,
  value,
  defaultValue,
  ...rest
}: Readonly<Props>) => {
  const handleChange: SearchProps['onChange'] = ({ target }) => {
    const value = target.value
    onChange?.(isEmptyString(value) ? null : value, target)
  }

  return (
    <CarbonSearch
      onChange={handleChange}
      value={value ?? ''}
      {...rest}
    />
  )
}

export default SearchInput
