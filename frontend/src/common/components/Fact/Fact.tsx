import type { PropsWithChildren } from 'react'
import clsx from 'clsx'

type Props = {
  title: string
  inline?: boolean
}

const Fact = ({
  children,
  title,
  inline,
}: Readonly<PropsWithChildren<Props>>) => {
  return (
    <div
      className={clsx('flex', {
        'flex-col': !inline,
      })}
    >
      <span className='text-sm text-carbon-gray-40'>{title}</span>
      {children}
    </div>
  )
}

export default Fact
