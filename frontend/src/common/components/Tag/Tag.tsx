import type { PropsWithChildren } from 'react'

/**
 * The component using the carbon tag relies on React context, making it unsuitable for server-side components,
 * I don't need to support multiple themes.
 */
const Tag = ({ children }: Readonly<PropsWithChildren>) => (
  <div className='text-xs text-carbon-gray-80 bg-carbon-gray-20 py-1 px-2 rounded-2xl w-fit'>
    {children}
  </div>
)

export default Tag
