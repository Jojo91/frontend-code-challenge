export type ComboboxValue<TItemType> = TItemType | null

export type ComboboxOnChangeHandler<TItemType> = (
  value: ComboboxValue<TItemType>,
) => void
