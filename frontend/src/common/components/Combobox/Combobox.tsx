import { ComboBox as CarbonCombobox } from '@carbon/react/'
import type { ComponentProps } from 'react'
import type { ComboboxValue, ComboboxOnChangeHandler } from './types'

type CarbonComboboxProps<TItemType> = ComponentProps<
  typeof CarbonCombobox<TItemType>
>

type Props<TItemType> = {
  onChange?: ComboboxOnChangeHandler<TItemType>
  value?: ComboboxValue<TItemType>
} & Omit<CarbonComboboxProps<TItemType>, 'onChange' | 'value'>

const Combobox = <TItemType,>({
  onChange,
  value,
  ...rest
}: Readonly<Props<TItemType>>) => {
  const handleChange: CarbonComboboxProps<TItemType>['onChange'] = (value) => {
    onChange?.(value.selectedItem ?? null)
  }

  return (
    <CarbonCombobox<TItemType>
      onChange={handleChange}
      selectedItem={value}
      {...rest}
    />
  )
}

export default Combobox
