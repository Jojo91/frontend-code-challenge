export const isEmptyString = (value: unknown): value is '' => value === ''
