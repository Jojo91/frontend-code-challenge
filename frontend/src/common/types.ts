export interface NextPageProps<
  TParams extends Record<string, string> = Record<string, string>,
  TSearchParams extends Record<string, string> = Record<string, string>,
> {
  params: Readonly<TParams>
  searchParams: Readonly<TSearchParams>
}

export interface NextErrorProps<TError = unknown> {
  error: TError
  // Attempts to recover by trying to re-render the segment
  reset: () => void
}
