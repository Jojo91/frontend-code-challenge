import clsx from 'clsx'
import Link from 'next/link'
import { ArrowRight } from '@carbon/react/icons'
import { POKEMON_EVOLUTION_FRAGMENT } from './fragments'
import Image from '@/common/components/Image'
import { type FragmentType, getFragmentData } from '@/__generated__'

type Props = {
  data: FragmentType<typeof POKEMON_EVOLUTION_FRAGMENT>
}

const EvolutionItem = ({ data }: Readonly<Props>) => {
  const { image, name } = getFragmentData(POKEMON_EVOLUTION_FRAGMENT, data)
  return (
    <Link
      href={`/${name}`}
      className={clsx(
        'flex bg-white hover:bg-carbon-gray-10 shadow-carbon-tile-top relative',
      )}
    >
      <span className='absolute w-full h-full shadow-carbon-tile-bottom top-0 left-0 pointer-events-none' />
      <div className='border-r border-solid border-carbon-gray-20 p-[1px]'>
        <Image
          src={image}
          className='w-44 h-44'
          alt={`${name}-image`}
        />
      </div>

      <div className='flex flex-col p-4 justify-between w-full'>
        <h1 className='text-3xl'>{name}</h1>

        <div className='flex justify-end text-blue-500'>
          <ArrowRight size={20} />
        </div>
      </div>
    </Link>
  )
}

export default EvolutionItem
