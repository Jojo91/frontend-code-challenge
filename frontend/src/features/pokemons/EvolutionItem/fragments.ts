import { gql } from '@/__generated__'

export const POKEMON_EVOLUTION_FRAGMENT = gql(/* GraphQL */ `
  fragment PokemonEvolution_Pokemon on Pokemon {
    id
    name
    image
  }
`)
