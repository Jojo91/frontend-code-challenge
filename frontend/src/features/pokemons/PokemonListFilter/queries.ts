import { gql } from '@/__generated__'

export const POKEMON_TYPES_QUERY = gql(/* GraphQL */ `
  query PokemonTypes {
    pokemonTypes
  }
`)
