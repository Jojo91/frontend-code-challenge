import type { SearchInputValue } from '@/common/components/SearchInput'
import type { ComboboxValue } from '@/common/components/Combobox'

export type PokemonListFilterValue = {
  search: SearchInputValue
  type: ComboboxValue<string>
}

export type PokemonListFilterOnChangeHandler = (
  value: PokemonListFilterValue,
) => void
