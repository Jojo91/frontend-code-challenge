'use client'

import { useSuspenseQuery } from '@apollo/experimental-nextjs-app-support/ssr'
import { useEffect, useState } from 'react'
import clsx from 'clsx'
import type {
  PokemonListFilterValue,
  PokemonListFilterOnChangeHandler,
} from './types'
import { POKEMON_TYPES_QUERY } from './queries'
import SearchInput, {
  type SearchInputValue,
} from '@/common/components/SearchInput'
import Combobox, {
  type ComboboxOnChangeHandler,
} from '@/common/components/Combobox'
import useDebounce from '@/common/hooks/useDebounce'

type Props = {
  value: PokemonListFilterValue
  onChange: PokemonListFilterOnChangeHandler
  className?: string
}

const PokemonListFilter = ({ value, onChange, className }: Readonly<Props>) => {
  const { data } = useSuspenseQuery(POKEMON_TYPES_QUERY)
  const [searchValue, setSearchValue] = useState<SearchInputValue>(null)
  const debouncedSearchValue = useDebounce(searchValue, 250)

  useEffect(() => {
    onChange({ ...value, search: debouncedSearchValue })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedSearchValue])

  const onComboboxChange: ComboboxOnChangeHandler<string> = (type) => {
    onChange({ ...value, type })
  }

  return (
    <div className={clsx('flex gap-3', className)}>
      <SearchInput
        labelText=''
        value={searchValue}
        id='pokemon-search'
        onChange={setSearchValue}
        className='bg-white'
      />

      <Combobox<string>
        id='pokemon-type'
        placeholder='Type'
        value={value.type}
        items={data.pokemonTypes}
        onChange={onComboboxChange}
      />
    </div>
  )
}

export default PokemonListFilter
