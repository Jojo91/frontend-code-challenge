'use client'

import { useState, Suspense, useEffect } from 'react'
import { Tabs, TabList, Tab, TabPanel, TabPanels } from '@carbon/react'
import PokemonListFilter, {
  type PokemonListFilterValue,
} from './PokemonListFilter'

import PokemonList from './PokemonList'
import LayoutSwitch, { LayoutSwitchValue } from './PokemonList/LayoutSwitch'
import FavoritePokemonList from './PokemonList/FavoritePokemonList'
import PokemonListLayout from './PokemonList/PokemonListLayout'
import ScrollToTopButton from './ScrollToTopButton'
import useScroll from '@/common/hooks/useScroll'

const PokemonLayout = () => {
  const { setScrollerRef, show: showScrollButton, goTop } = useScroll()
  const [filter, setFilter] = useState<PokemonListFilterValue>({
    type: null,
    search: null,
  })

  const [layout, setLayout] = useState<LayoutSwitchValue>('grid')

  // Every time our filter changes, it begins at the start of the list.
  useEffect(() => {
    goTop('instant')
  }, [filter.type, filter.search, goTop])

  return (
    <Tabs>
      <TabList
        className='bg-carbon-gray-20'
        aria-label='list-of-pokemons'
        contained
      >
        <Tab className='bg-black'>All</Tab>
        <Tab>Favorites</Tab>
      </TabList>

      <div className='p-4 shadow z-10'>
        <div className='container mx-auto flex justify-between '>
          <PokemonListFilter
            value={filter}
            onChange={setFilter}
          />
          <LayoutSwitch
            value={layout}
            onChange={setLayout}
          />
        </div>
      </div>

      <ScrollToTopButton
        show={showScrollButton}
        onClick={() => goTop()}
      />

      <div
        className='overflow-y-auto'
        ref={setScrollerRef}
      >
        <div className='container mx-auto'>
          <TabPanels>
            <TabPanel>
              <Suspense
                fallback={
                  <PokemonListLayout
                    layout={layout}
                    loading
                  />
                }
              >
                <PokemonListLayout layout={layout}>
                  <PokemonList
                    search={filter.search}
                    type={filter.type}
                    inline={layout === 'list'}
                  />
                </PokemonListLayout>
              </Suspense>
            </TabPanel>

            <TabPanel>
              <Suspense
                fallback={
                  <PokemonListLayout
                    layout={layout}
                    loading
                  />
                }
              >
                <PokemonListLayout layout={layout}>
                  <FavoritePokemonList
                    search={filter.search}
                    type={filter.type}
                    inline={layout === 'list'}
                  />
                </PokemonListLayout>
              </Suspense>
            </TabPanel>
          </TabPanels>
        </div>
      </div>
    </Tabs>
  )
}

export default PokemonLayout
