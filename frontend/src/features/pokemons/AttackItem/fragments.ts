import { gql } from '@/__generated__'

export const ATTACK_FRAGMENT = gql(/* GraphQL */ `
  fragment PokemonAttack_Attack on Attack {
    name
    damage
    type
  }
`)
