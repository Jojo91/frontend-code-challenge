import { ATTACK_FRAGMENT } from './fragments'
import Tag from '@/common/components/Tag'
import Fact from '@/common/components/Fact/Fact'
import { type FragmentType, getFragmentData } from '@/__generated__'

type Props = {
  data: FragmentType<typeof ATTACK_FRAGMENT>
}

const AttackItem = ({ data }: Readonly<Props>) => {
  const attack = getFragmentData(ATTACK_FRAGMENT, data)

  return (
    <div className='shadow-carbon-tile-top p-4 bg-white relative'>
      <span className='absolute top-0 left-0 w-full h-full shadow-carbon-tile-bottom pointer-events-none' />
      <h1 className='text-md font-bold mb-1'>{attack.name}</h1>

      <Tag>{attack.type}</Tag>

      <div className='flex flex-row justify-end items-baseline gap-1'>
        <span className='text-4xl mt-4'>{attack.damage}</span>
        <span className='text-sm text-carbon-gray-60'>Dmg</span>
      </div>
    </div>
  )
}

export default AttackItem
