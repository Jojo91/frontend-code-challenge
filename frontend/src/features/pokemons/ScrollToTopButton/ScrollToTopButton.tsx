import clsx from 'clsx'
import { UpToTop } from '@carbon/react/icons'
import type { MouseEventHandler } from 'react'

type Props = {
  show: boolean
  onClick: MouseEventHandler<HTMLButtonElement>
}

const ScrollToTopButton = ({ show, onClick }: Readonly<Props>) => (
  <button
    type='button'
    onClick={onClick}
    className={clsx(
      'absolute right-10 z-10 bottom-10 bg-carbon-gray-60 hover:bg-carbon-gray-80 rounded-full text-carbon-gray-10 w-14 h-14 flex items-center justify-center ',
      show ? 'animate-jump-in' : 'animate-jump-out',
    )}
  >
    <UpToTop size={30} />
  </button>
)

export default ScrollToTopButton
