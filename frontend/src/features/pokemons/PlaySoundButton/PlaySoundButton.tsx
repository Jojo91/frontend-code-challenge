'use client'

import { VolumeUp, VolumeUpFilled } from '@carbon/react/icons'
import clsx from 'clsx'
import useAudio from '@/common/hooks/useAudio'

type Props = {
  url: string
}

const PlaySoundButton = ({ url }: Readonly<Props>) => {
  const { isPlaying, isError, play } = useAudio(url)

  const VolumeIcon = isPlaying ? VolumeUpFilled : VolumeUp
  return (
    <button
      type='button'
      onClick={play}
      disabled={isPlaying}
      className={clsx('text-carbon-gray-10 ', {
        'hover:text-blue-600': !isPlaying,
      })}
    >
      <VolumeIcon size={35} />
    </button>
  )
}

export default PlaySoundButton
