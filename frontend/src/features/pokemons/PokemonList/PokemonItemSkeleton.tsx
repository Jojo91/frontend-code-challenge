import { SkeletonPlaceholder } from '@carbon/react'
import clsx from 'clsx'

type Props = {
  inline?: boolean
}

const PokemonItemSkeleton = ({ inline }: Readonly<Props>) => (
  <SkeletonPlaceholder
    className={clsx('w-full h-64', inline ? 'h-64' : 'h-96')}
  />
)

export default PokemonItemSkeleton
