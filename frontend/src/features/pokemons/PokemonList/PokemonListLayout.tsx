import clsx from 'clsx'
import type { PropsWithChildren } from 'react'
import type { LayoutSwitchValue } from '@/features/pokemons/PokemonList/LayoutSwitch/types'
import PokemonItemSkeleton from '@/features/pokemons/PokemonList/PokemonItemSkeleton'

type Layout = 'list' | 'grid'

const layoutClassNamesMap: Record<Layout, string> = {
  list: 'lg:grid-cols-1',
  grid: 'lg:grid-cols-3',
}

type Props = {
  layout: LayoutSwitchValue
  loading?: boolean
}

const PokemonListLayout = ({
  children,
  layout,
  loading = false,
}: Readonly<PropsWithChildren<Props>>) => (
  <div className={clsx('grid gap-8 w-full my-4', layoutClassNamesMap[layout])}>
    {loading
      ? [...Array(3).keys()].map((key) => (
          <PokemonItemSkeleton
            key={key}
            inline={layout === 'list'}
          />
        ))
      : children}
  </div>
)

export default PokemonListLayout
