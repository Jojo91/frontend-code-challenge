import { useSuspenseQuery } from '@apollo/experimental-nextjs-app-support/ssr'
import { FAVORITE_POKEMONS_QUERY } from './queries'
import PokemonItem from '@/features/pokemons/PokemonList/PokemonItem'

type Props = {
  search: string | null
  type: string | null
  inline?: boolean
}

const FavoritePokemonList = ({
  search,
  type,
  inline = false,
}: Readonly<Props>) => {
  const { data } = useSuspenseQuery(FAVORITE_POKEMONS_QUERY, {
    variables: {
      search,
      type,
    },
  })

  const isEmpty = data.pokemons.edges.length === 0

  return isEmpty ? (
    <div className='col-span-3 flex items-center justify-center h-64 text-3xl text-carbon-gray-30'>
      Favorite list is empty.
    </div>
  ) : (
    data.pokemons.edges.map((pokemon, index) => (
      <PokemonItem
        key={pokemon.id}
        data={pokemon}
        inline={inline}
      />
    ))
  )
}

export default FavoritePokemonList
