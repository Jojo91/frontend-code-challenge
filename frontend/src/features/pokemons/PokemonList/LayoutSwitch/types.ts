export type LayoutSwitchValue = 'list' | 'grid'

export type LayoutSwitchOnChangeHandler = (value: LayoutSwitchValue) => void
