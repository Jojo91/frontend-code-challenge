import { Grid, Menu } from '@carbon/react/icons'
import type { MouseEventHandler } from 'react'
import clsx from 'clsx'
import type { LayoutSwitchOnChangeHandler, LayoutSwitchValue } from './types'

type Props = {
  value?: LayoutSwitchValue
  onChange?: LayoutSwitchOnChangeHandler
}

const LayoutSwitch = ({ onChange, value = 'list' }: Readonly<Props>) => {
  const handleClick =
    (type: LayoutSwitchValue): MouseEventHandler<HTMLButtonElement> =>
    () => {
      onChange?.(type)
    }

  return (
    <div className='gap-2 hidden lg:flex'>
      <button
        type='button'
        onClick={handleClick('list')}
        className={clsx({
          'text-blue-700': value === 'list',
        })}
      >
        <Menu size={24} />
      </button>

      <button
        type='button'
        onClick={handleClick('grid')}
        className={clsx({
          'text-blue-700': value === 'grid',
        })}
      >
        <Grid size={24} />
      </button>
    </div>
  )
}

export default LayoutSwitch
