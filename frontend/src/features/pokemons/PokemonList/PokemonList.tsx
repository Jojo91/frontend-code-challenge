import { useSuspenseQuery } from '@apollo/experimental-nextjs-app-support/ssr'
import { useCallback, useTransition } from 'react'
import { InView } from 'react-intersection-observer'
import { All_POKEMONS_QUERY } from './queries'
import PokemonItem from './PokemonItem'
import PokemonItemSkeleton from '@/features/pokemons/PokemonList/PokemonItemSkeleton'

type Props = {
  search: string | null
  type: string | null
  inline?: boolean
}

const PokemonList = ({ search, type, inline }: Readonly<Props>) => {
  const [isPending, startTransition] = useTransition()
  const { data, fetchMore } = useSuspenseQuery(All_POKEMONS_QUERY, {
    variables: {
      query: {
        limit: 12,
        offset: 0,
        search: search,
        filter: { type: type },
      },
    },
  })

  const loadMore = useCallback(() => {
    if (data.pokemons.edges.length === data.pokemons.count) {
      return
    }

    startTransition(() => {
      void fetchMore({
        variables: {
          query: {
            search: search,
            filter: { type: type },
            offset: data.pokemons.edges.length,
          },
        },
      })
    })
  }, [data.pokemons.count, data.pokemons.edges.length, fetchMore, search, type])

  const onInView = async (inView: boolean) => {
    if (!inView) return

    loadMore()
  }

  const isEmpty = data.pokemons.edges.length === 0

  return isEmpty ? (
    <div className='col-span-3 flex items-center justify-center h-64 text-3xl text-carbon-gray-30'>
      List is empty.
    </div>
  ) : (
    <>
      {data.pokemons.edges.map((pokemon, index) => (
        <PokemonItem
          key={pokemon.id}
          data={pokemon}
          inline={inline}
        />
      ))}

      {isPending ? (
        <PokemonItemSkeleton inline />
      ) : (
        <InView onChange={onInView}>
          <div className='h-14' />
        </InView>
      )}
    </>
  )
}

export default PokemonList
