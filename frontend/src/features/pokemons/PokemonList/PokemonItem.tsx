import Link from 'next/link'
import clsx from 'clsx'
import type { PropsWithChildren } from 'react'
import { POKEMON_FRAGMENT } from './fragments'
import PokemonTypeList from '@/features/pokemons/PokemonTypeList'
import { type FragmentType, getFragmentData } from '@/__generated__'
import Image from '@/common/components/Image'
import PokemonDetailModalButton from '@/features/pokemons/PokemonDetailModalButton'
import FavoriteButton from '@/features/pokemons/FavoriteButton'

type Props = {
  data: FragmentType<typeof POKEMON_FRAGMENT>
  inline?: boolean
}

const PokemonItem = ({
  inline = false,
  data,
  children,
}: Readonly<PropsWithChildren<Props>>) => {
  const pokemon = getFragmentData(POKEMON_FRAGMENT, data)

  return (
    <div
      className={clsx(
        'bg-carbon-gray-10 shadow-carbon-tile-bottom flex relative',
        inline ? 'flex-row' : 'flex-col',
      )}
    >
      <span className='absolute w-full h-full shadow-carbon-tile-top top-0 left-0 pointer-events-none' />
      <Link
        prefetch
        href={`/${pokemon.name}`}
        className={clsx(
          'flex justify-center items-center p-4 bg-white border-solid border-carbon-gray-20',
          inline ? 'border-r' : 'border-b',
        )}
      >
        <Image
          src={pokemon.image}
          alt={`${pokemon.name}-image`}
          className='w-64 h-64'
        />
      </Link>

      {/* Item Header */}
      <div className='flex flex-col w-full p-4 '>
        <div className='flex flex-col gap-1'>
          <span className='text-sm text-carbon-gray-70 -mb-2'>
            {pokemon.classification}
          </span>

          <Link
            prefetch
            href={`/${pokemon.name}`}
            className='text-3xl text-blue-500 hover:text-blue-700'
          >
            {pokemon.name}
          </Link>

          <PokemonTypeList data={pokemon?.types} />
        </div>

        {/* Item actions*/}
        <div className='flex items-end gap-2 self-end h-full'>
          <PokemonDetailModalButton pokemonId={pokemon.id} />
          <FavoriteButton
            pokemonId={pokemon.id}
            isFavorite={pokemon.isFavorite}
          />
        </div>
      </div>
    </div>
  )
}

export default PokemonItem
