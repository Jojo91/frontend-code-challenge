import { gql } from '@/__generated__'

export const POKEMON_FRAGMENT = gql(/* GraphQL */ `
  fragment PokomenItem_Pokemon on Pokemon {
    id
    number
    classification
    name
    image
    isFavorite
    sound
    types
  }
`)
