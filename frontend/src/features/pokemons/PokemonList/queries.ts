import { gql } from '@/__generated__'

export const All_POKEMONS_QUERY = gql(/* GraphQL */ `
  query AllPokemons($query: PokemonsQueryInput!) {
    pokemons(query: $query) {
      offset
      limit
      count
      edges {
        id
        ...PokomenItem_Pokemon
        ...FavoritButton_Pokemon
        ...PokemonDetailModal_Pokemon
      }
    }
  }
`)

export const FAVORITE_POKEMONS_QUERY = gql(/* GraphQL */ `
  query FavoritePokemons($search: String, $type: String) {
    pokemons(
      query: { search: $search, filter: { type: $type, isFavorite: true } }
    ) {
      offset
      limit
      count
      edges {
        id
        ...PokomenItem_Pokemon
        ...FavoritButton_Pokemon
        ...PokemonDetailModal_Pokemon
      }
    }
  }
`)
