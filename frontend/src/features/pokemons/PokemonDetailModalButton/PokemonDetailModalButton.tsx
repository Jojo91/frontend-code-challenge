'use client'

import { View } from '@carbon/react/icons'
import PokemonDetailModal from './DetailModal'
import useToggle from '@/common/hooks/useToggle'

type Props = {
  pokemonId: string
}

const PokemonDetailModalButton = ({ pokemonId }: Readonly<Props>) => {
  const [isOpen, toggle] = useToggle()

  return (
    <>
      <button
        type='button'
        onClick={toggle}
        className='hover:text-blue-500'
      >
        <View size={40} />
      </button>

      <PokemonDetailModal
        open={isOpen}
        onClose={toggle}
        pokemonId={pokemonId}
      />
    </>
  )
}

export default PokemonDetailModalButton
