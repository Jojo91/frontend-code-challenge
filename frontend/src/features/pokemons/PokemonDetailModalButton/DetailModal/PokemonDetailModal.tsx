import { Modal } from '@carbon/react'
import { createPortal } from 'react-dom'
import PokemonDetailModalContent from './PokemonDetailModalContent'

type Props = {
  open: boolean
  onClose: () => void
  pokemonId: string
}

const PokemonDetailModal = ({ open, onClose, pokemonId }: Readonly<Props>) => {
  return open
    ? createPortal(
        <Modal
          open
          onRequestClose={onClose}
          modalLabel='Combat Capabilities'
          passiveModal
        >
          <PokemonDetailModalContent pokemonId={pokemonId} />
        </Modal>,
        document.body,
      )
    : null
}

export default PokemonDetailModal
