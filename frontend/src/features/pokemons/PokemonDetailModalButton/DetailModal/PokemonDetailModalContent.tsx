import { useFragment } from '@apollo/experimental-nextjs-app-support/ssr'
import { POKEMON_DETAIL_MODAL_FRAGMENT } from './fragments'
import Title from '@/common/components/Title'
import PokemonTypeList from '@/features/pokemons/PokemonTypeList'
import AttackItem from '@/features/pokemons/AttackItem'

type Props = {
  pokemonId: string
}

const PokemonDetailModalContent = ({ pokemonId }: Readonly<Props>) => {
  const { complete, data: pokemon } = useFragment({
    fragment: POKEMON_DETAIL_MODAL_FRAGMENT,
    from: {
      __typename: 'Pokemon',
      id: pokemonId,
    },
    fragmentName: 'PokemonDetailModal_Pokemon',
  })

  return complete ? (
    <div className='flex flex-col gap-5'>
      <div>
        <Title>Weaknesses</Title>
        <PokemonTypeList
          data={pokemon.weaknesses}
          tile
        />
      </div>

      <div>
        <Title>Resistances</Title>
        <PokemonTypeList
          data={pokemon.resistant}
          tile
        />
      </div>
      <div>
        <Title>Fast Attacks</Title>
        <div className='grid grid-cols-3'>
          {pokemon.attacks.fast.map((attack) => (
            <AttackItem
              key={attack.name}
              data={attack}
            />
          ))}
        </div>
      </div>

      <div>
        <Title>Special Attacks</Title>
        <div className='grid grid-cols-3'>
          {pokemon.attacks.special.map((attack) => (
            <AttackItem
              key={attack.name}
              data={attack}
            />
          ))}
        </div>
      </div>
    </div>
  ) : (
    <div>NOthing</div>
  )
}

export default PokemonDetailModalContent
