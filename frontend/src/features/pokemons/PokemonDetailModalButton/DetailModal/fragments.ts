import { gql } from '@/__generated__'

export const POKEMON_DETAIL_MODAL_FRAGMENT = gql(/* GraphQL */ `
  fragment PokemonDetailModal_Pokemon on Pokemon {
    id
    weaknesses
    resistant
    attacks {
      fast {
        name
        ...PokemonAttack_Attack
      }

      special {
        name
        ...PokemonAttack_Attack
      }
    }
  }
`)
