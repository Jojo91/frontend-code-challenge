import MyTag from '@/common/components/Tag'

type Props = {
  data: string[] | undefined
  tile?: boolean
}

const PokemonTypeList = ({ data = [], tile = false }: Readonly<Props>) => (
  <div className={tile ? 'grid grid-cols-4' : 'flex gap-1'}>
    {data.map((type) =>
      tile ? (
        <h1
          key={type}
          className='text-md font-bold p-4 bg-white w-full relative shadow-carbon-tile-top'
        >
          <span className='absolute w-full h-full shadow-carbon-tile-bottom top-0 left-0' />
          {type}
        </h1>
      ) : (
        <MyTag key={type}>{type}</MyTag>
      ),
    )}
  </div>
)

export default PokemonTypeList
