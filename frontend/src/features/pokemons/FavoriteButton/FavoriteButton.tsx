'use client'

import { Favorite, FavoriteFilled } from '@carbon/react/icons'
import clsx from 'clsx'
import useToggleFavorite, {
  type UseToggleFavoriteOptions,
} from './useToggleFavorite'

type Props = UseToggleFavoriteOptions & {
  // Use a light color scheme.
  light?: boolean
}

const FavoriteButton = ({ light, ...rest }: Readonly<Props>) => {
  const [toggle, { loading }] = useToggleFavorite(rest)

  const FavoriteIcon = rest.isFavorite ? FavoriteFilled : Favorite

  return (
    <button
      type='button'
      onClick={toggle}
      className={clsx(
        'hover:text-blue-500',
        light ? 'text-carbon-gray-10 ' : undefined,
      )}
    >
      <FavoriteIcon size={40} />
    </button>
  )
}

export default FavoriteButton
