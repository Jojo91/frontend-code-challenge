import { gql } from '@/__generated__'

export const FAVORITE_POKEMON_MUTATION = gql(/* GraphQL */ `
  mutation FavoritePokemon($id: ID!) {
    favoritePokemon(id: $id) {
      id
      name
      isFavorite
    }
  }
`)
export const UN_FAVORITE_POKEMON_MUTATION = gql(/* GraphQL */ `
  mutation UnFavoritePokemon($id: ID!) {
    unFavoritePokemon(id: $id) {
      id
      name
      isFavorite
    }
  }
`)
