import { useMutation } from '@apollo/client'
import { useCallback } from 'react'
import { toast } from 'react-toastify'
import { useRouter } from 'next/navigation'
import {
  FAVORITE_POKEMON_MUTATION,
  UN_FAVORITE_POKEMON_MUTATION,
} from './mutations'
import type {
  FavoritePokemonMutation,
  UnFavoritePokemonMutation,
} from '@/__generated__/graphql'

export type UseToggleFavoriteOptions = {
  pokemonId: string
  isFavorite: boolean
  // Invoked from a React server component.
  rsc?: boolean
}

const useToggleFavorite = ({
  pokemonId,
  isFavorite,
  rsc,
}: UseToggleFavoriteOptions) => {
  const router = useRouter()

  const notify = (
    pokemon:
      | UnFavoritePokemonMutation['unFavoritePokemon']
      | FavoritePokemonMutation['favoritePokemon'],
  ) => {
    if (pokemon == null) return

    toast(
      pokemon.isFavorite
        ? `${pokemon.name} has been put into favorite list.`
        : `${pokemon.name} has been removed from favorite list.`,
      { type: pokemon.isFavorite ? 'success' : 'info' },
    )

    // When in RSC to get new data, simply refresh the entire page.
    // Cache update or re-fetching the query in mutation won't work since we didn't fetch it on the client side.
    if (rsc) {
      router.refresh()
    }
  }

  // Will refetch all queries with this query name. There can be many as we use be filters.
  const refetchQueries = () => ['FavoritePokemons']

  const [favoritePokemon, favoriteResult] = useMutation(
    FAVORITE_POKEMON_MUTATION,
    {
      refetchQueries,
      onCompleted: ({ favoritePokemon }) => {
        notify(favoritePokemon)
      },
      update: (cache, { data }) => {
        // Update pokemon entity in cache
        cache.modify({
          id: pokemonId,
          fields: {
            isFavorite: (cachedIsFavorite) =>
              data?.favoritePokemon?.isFavorite ?? cachedIsFavorite,
          },
        })
      },
    },
  )
  const [unFavoritePokemon, unFavoriteResult] = useMutation(
    UN_FAVORITE_POKEMON_MUTATION,
    {
      refetchQueries,
      onCompleted: ({ unFavoritePokemon }) => {
        notify(unFavoritePokemon)
      },
      update: (cache, { data }) => {
        // Update pokemon entity in cache
        cache.modify({
          id: pokemonId,
          broadcast: true,
          fields: {
            isFavorite: (cachedIsFavorite) =>
              data?.unFavoritePokemon?.isFavorite ?? cachedIsFavorite,
          },
        })
      },
    },
  )

  const toggle = useCallback(() => {
    const mutation = isFavorite ? unFavoritePokemon : favoritePokemon

    mutation({ variables: { id: pokemonId } })
  }, [favoritePokemon, isFavorite, pokemonId, unFavoritePokemon])

  return [
    toggle,
    { loading: unFavoriteResult.loading || favoriteResult.loading },
  ] as const
}

export default useToggleFavorite
