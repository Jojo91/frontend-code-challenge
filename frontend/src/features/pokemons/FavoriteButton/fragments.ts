import { gql } from '@/__generated__'

export const FAVORITE_BUTTON_FRAGMENT = gql(/* GraphQL */ `
  fragment FavoritButton_Pokemon on Pokemon {
    id
    isFavorite
  }
`)
