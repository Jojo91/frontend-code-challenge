/* eslint-disable */
import * as types from './graphql';
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';

/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel or swc plugin for production.
 */
const documents = {
    "\n  query PokemonDetail($name: String!) {\n    pokemonByName(name: $name) {\n      id\n      name\n      maxHP\n      maxCP\n      image\n      sound\n      classification\n      number\n\n      evolutions {\n        id\n        ...PokemonEvolution_Pokemon\n      }\n\n      attacks {\n        fast {\n          name\n          ...PokemonAttack_Attack\n        }\n        special {\n          name\n          ...PokemonAttack_Attack\n        }\n      }\n\n      evolutionRequirements {\n        name\n        amount\n      }\n\n      height {\n        maximum\n        minimum\n      }\n      weight {\n        maximum\n        minimum\n      }\n      weaknesses\n      types\n      isFavorite\n      resistant\n      fleeRate\n    }\n  }\n": types.PokemonDetailDocument,
    "\n  fragment PokemonAttack_Attack on Attack {\n    name\n    damage\n    type\n  }\n": types.PokemonAttack_AttackFragmentDoc,
    "\n  fragment PokemonEvolution_Pokemon on Pokemon {\n    id\n    name\n    image\n  }\n": types.PokemonEvolution_PokemonFragmentDoc,
    "\n  fragment FavoritButton_Pokemon on Pokemon {\n    id\n    isFavorite\n  }\n": types.FavoritButton_PokemonFragmentDoc,
    "\n  mutation FavoritePokemon($id: ID!) {\n    favoritePokemon(id: $id) {\n      id\n      name\n      isFavorite\n    }\n  }\n": types.FavoritePokemonDocument,
    "\n  mutation UnFavoritePokemon($id: ID!) {\n    unFavoritePokemon(id: $id) {\n      id\n      name\n      isFavorite\n    }\n  }\n": types.UnFavoritePokemonDocument,
    "\n  fragment PokemonDetailModal_Pokemon on Pokemon {\n    id\n    weaknesses\n    resistant\n    attacks {\n      fast {\n        name\n        ...PokemonAttack_Attack\n      }\n\n      special {\n        name\n        ...PokemonAttack_Attack\n      }\n    }\n  }\n": types.PokemonDetailModal_PokemonFragmentDoc,
    "\n  fragment PokomenItem_Pokemon on Pokemon {\n    id\n    number\n    classification\n    name\n    image\n    isFavorite\n    sound\n    types\n  }\n": types.PokomenItem_PokemonFragmentDoc,
    "\n  query AllPokemons($query: PokemonsQueryInput!) {\n    pokemons(query: $query) {\n      offset\n      limit\n      count\n      edges {\n        id\n        ...PokomenItem_Pokemon\n        ...FavoritButton_Pokemon\n        ...PokemonDetailModal_Pokemon\n      }\n    }\n  }\n": types.AllPokemonsDocument,
    "\n  query FavoritePokemons($search: String, $type: String) {\n    pokemons(\n      query: { search: $search, filter: { type: $type, isFavorite: true } }\n    ) {\n      offset\n      limit\n      count\n      edges {\n        id\n        ...PokomenItem_Pokemon\n        ...FavoritButton_Pokemon\n        ...PokemonDetailModal_Pokemon\n      }\n    }\n  }\n": types.FavoritePokemonsDocument,
    "\n  query PokemonTypes {\n    pokemonTypes\n  }\n": types.PokemonTypesDocument,
};

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 *
 *
 * @example
 * ```ts
 * const query = gql(`query GetUser($id: ID!) { user(id: $id) { name } }`);
 * ```
 *
 * The query argument is unknown!
 * Please regenerate the types.
 */
export function gql(source: string): unknown;

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query PokemonDetail($name: String!) {\n    pokemonByName(name: $name) {\n      id\n      name\n      maxHP\n      maxCP\n      image\n      sound\n      classification\n      number\n\n      evolutions {\n        id\n        ...PokemonEvolution_Pokemon\n      }\n\n      attacks {\n        fast {\n          name\n          ...PokemonAttack_Attack\n        }\n        special {\n          name\n          ...PokemonAttack_Attack\n        }\n      }\n\n      evolutionRequirements {\n        name\n        amount\n      }\n\n      height {\n        maximum\n        minimum\n      }\n      weight {\n        maximum\n        minimum\n      }\n      weaknesses\n      types\n      isFavorite\n      resistant\n      fleeRate\n    }\n  }\n"): (typeof documents)["\n  query PokemonDetail($name: String!) {\n    pokemonByName(name: $name) {\n      id\n      name\n      maxHP\n      maxCP\n      image\n      sound\n      classification\n      number\n\n      evolutions {\n        id\n        ...PokemonEvolution_Pokemon\n      }\n\n      attacks {\n        fast {\n          name\n          ...PokemonAttack_Attack\n        }\n        special {\n          name\n          ...PokemonAttack_Attack\n        }\n      }\n\n      evolutionRequirements {\n        name\n        amount\n      }\n\n      height {\n        maximum\n        minimum\n      }\n      weight {\n        maximum\n        minimum\n      }\n      weaknesses\n      types\n      isFavorite\n      resistant\n      fleeRate\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  fragment PokemonAttack_Attack on Attack {\n    name\n    damage\n    type\n  }\n"): (typeof documents)["\n  fragment PokemonAttack_Attack on Attack {\n    name\n    damage\n    type\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  fragment PokemonEvolution_Pokemon on Pokemon {\n    id\n    name\n    image\n  }\n"): (typeof documents)["\n  fragment PokemonEvolution_Pokemon on Pokemon {\n    id\n    name\n    image\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  fragment FavoritButton_Pokemon on Pokemon {\n    id\n    isFavorite\n  }\n"): (typeof documents)["\n  fragment FavoritButton_Pokemon on Pokemon {\n    id\n    isFavorite\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  mutation FavoritePokemon($id: ID!) {\n    favoritePokemon(id: $id) {\n      id\n      name\n      isFavorite\n    }\n  }\n"): (typeof documents)["\n  mutation FavoritePokemon($id: ID!) {\n    favoritePokemon(id: $id) {\n      id\n      name\n      isFavorite\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  mutation UnFavoritePokemon($id: ID!) {\n    unFavoritePokemon(id: $id) {\n      id\n      name\n      isFavorite\n    }\n  }\n"): (typeof documents)["\n  mutation UnFavoritePokemon($id: ID!) {\n    unFavoritePokemon(id: $id) {\n      id\n      name\n      isFavorite\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  fragment PokemonDetailModal_Pokemon on Pokemon {\n    id\n    weaknesses\n    resistant\n    attacks {\n      fast {\n        name\n        ...PokemonAttack_Attack\n      }\n\n      special {\n        name\n        ...PokemonAttack_Attack\n      }\n    }\n  }\n"): (typeof documents)["\n  fragment PokemonDetailModal_Pokemon on Pokemon {\n    id\n    weaknesses\n    resistant\n    attacks {\n      fast {\n        name\n        ...PokemonAttack_Attack\n      }\n\n      special {\n        name\n        ...PokemonAttack_Attack\n      }\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  fragment PokomenItem_Pokemon on Pokemon {\n    id\n    number\n    classification\n    name\n    image\n    isFavorite\n    sound\n    types\n  }\n"): (typeof documents)["\n  fragment PokomenItem_Pokemon on Pokemon {\n    id\n    number\n    classification\n    name\n    image\n    isFavorite\n    sound\n    types\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query AllPokemons($query: PokemonsQueryInput!) {\n    pokemons(query: $query) {\n      offset\n      limit\n      count\n      edges {\n        id\n        ...PokomenItem_Pokemon\n        ...FavoritButton_Pokemon\n        ...PokemonDetailModal_Pokemon\n      }\n    }\n  }\n"): (typeof documents)["\n  query AllPokemons($query: PokemonsQueryInput!) {\n    pokemons(query: $query) {\n      offset\n      limit\n      count\n      edges {\n        id\n        ...PokomenItem_Pokemon\n        ...FavoritButton_Pokemon\n        ...PokemonDetailModal_Pokemon\n      }\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query FavoritePokemons($search: String, $type: String) {\n    pokemons(\n      query: { search: $search, filter: { type: $type, isFavorite: true } }\n    ) {\n      offset\n      limit\n      count\n      edges {\n        id\n        ...PokomenItem_Pokemon\n        ...FavoritButton_Pokemon\n        ...PokemonDetailModal_Pokemon\n      }\n    }\n  }\n"): (typeof documents)["\n  query FavoritePokemons($search: String, $type: String) {\n    pokemons(\n      query: { search: $search, filter: { type: $type, isFavorite: true } }\n    ) {\n      offset\n      limit\n      count\n      edges {\n        id\n        ...PokomenItem_Pokemon\n        ...FavoritButton_Pokemon\n        ...PokemonDetailModal_Pokemon\n      }\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query PokemonTypes {\n    pokemonTypes\n  }\n"): (typeof documents)["\n  query PokemonTypes {\n    pokemonTypes\n  }\n"];

export function gql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> = TDocumentNode extends DocumentNode<  infer TType,  any>  ? TType  : never;