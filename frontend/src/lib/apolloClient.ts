import { ApolloClient, InMemoryCache } from '@apollo/client'
import { registerApolloClient } from '@apollo/experimental-nextjs-app-support/rsc'
import env from './env'

console.log(env.GRAPHQL_URI)
const { getClient: apolloClient } = registerApolloClient(
  () =>
    new ApolloClient({
      uri: env.GRAPHQL_URI,
      cache: new InMemoryCache(),
    }),
)

export default apolloClient
