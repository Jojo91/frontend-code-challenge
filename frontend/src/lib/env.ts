interface CustomEnv {
  NEXT_PUBLIC_GRAPHQL_URI: string
}

declare global {
  namespace NodeJS {
    interface ProcessEnv extends CustomEnv {}
  }
}

const env = {
  GRAPHQL_URI: process.env.NEXT_PUBLIC_GRAPHQL_URI,
  NODE_ENV: process.env.NODE_ENV,
}

export default env
