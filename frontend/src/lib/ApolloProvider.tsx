'use client'
import type { PropsWithChildren } from 'react'
import { ApolloLink, HttpLink } from '@apollo/client'
import {
  NextSSRApolloClient,
  ApolloNextAppProvider,
  NextSSRInMemoryCache,
  SSRMultipartLink,
} from '@apollo/experimental-nextjs-app-support/ssr'
import { loadErrorMessages, loadDevMessages } from '@apollo/client/dev'
import { setVerbosity } from 'ts-invariant'
import env from './env'

if (env.NODE_ENV === 'development') {
  setVerbosity('debug')
  loadDevMessages()
  loadErrorMessages()
}

function makeClient() {
  const httpLink = new HttpLink({
    uri: env.GRAPHQL_URI,
    fetchOptions: { cache: 'no-store' },
  })

  const cache = new NextSSRInMemoryCache({
    typePolicies: {
      Query: {
        fields: {
          pokemons: {
            keyArgs: ['query', ['filter', ['type', 'isFavorite'], 'search']],
            merge: (existing, incoming, { args }) => {
              const offset = args?.query?.offset ?? 0
              const merged = existing?.edges ? existing.edges.slice(0) : []

              for (let i = 0; i < incoming.edges.length; ++i) {
                merged[offset + i] = incoming.edges[i]
              }

              return {
                ...incoming,
                edges: merged,
              }
            },
          },
        },
      },
    },
  })

  return new NextSSRApolloClient({
    connectToDevTools: env.NODE_ENV === 'development',
    cache,
    link:
      typeof window === 'undefined'
        ? ApolloLink.from([
            new SSRMultipartLink({
              stripDefer: true,
            }),
            httpLink,
          ])
        : httpLink,
  })
}

const ApolloWrapper = ({ children }: PropsWithChildren) => (
  <ApolloNextAppProvider makeClient={makeClient}>
    {children}
  </ApolloNextAppProvider>
)

export default ApolloWrapper
